json.array!(@surveysets) do |surveyset|
  json.extract! surveyset, :id, :name, :full_name, :category, :description, :license, :fees, :intervals, :scoring, :link_to_questions, :user_id, :number_of_questions, :time_to_complete, :completion_rate, :condition_type, :care_plans
  json.url surveyset_url(surveyset, format: :json)
end
