# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


$(document).ready ->

  # Show more info for each PROM upon click
  $('.expand').click (e) ->
    e.preventDefault()
    $(this).next().toggle()
    $(this).text if $(this).text() == 'Show less' then 'Show more' else 'Show less'
    return

  # Make filters work by searching for filter button's text in each PROM's category
  $('.filters').click ->
    $('.prom').css('display','none');
    $('.small_category:contains(' + $(this).text() + ')').parent().css('display','block');
    return

  # Clear filters
  $('#clear_filter').click ->
    $('.prom').css('display','block');
    return

  return
