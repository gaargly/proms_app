class SurveysetsController < ApplicationController
  before_action :set_surveyset, only: [:show, :edit, :update, :destroy]

  # GET /surveysets
  # GET /surveysets.json
  def index
    @surveysets = Surveyset.all
  end

  # GET /surveysets/1
  # GET /surveysets/1.json
  def show
  end

  # GET /surveysets/new
  def new
    @surveyset = Surveyset.new
  end

  # GET /surveysets/1/edit
  def edit
  end

  # POST /surveysets
  # POST /surveysets.json
  def create
    @surveyset = Surveyset.new(surveyset_params)

    respond_to do |format|
      if @surveyset.save
        format.html { redirect_to @surveyset, notice: 'Surveyset was successfully created.' }
        format.json { render :show, status: :created, location: @surveyset }
      else
        format.html { render :new }
        format.json { render json: @surveyset.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /surveysets/1
  # PATCH/PUT /surveysets/1.json
  def update
    respond_to do |format|
      if @surveyset.update(surveyset_params)
        format.html { redirect_to @surveyset, notice: 'Surveyset was successfully updated.' }
        format.json { render :show, status: :ok, location: @surveyset }
      else
        format.html { render :edit }
        format.json { render json: @surveyset.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /surveysets/1
  # DELETE /surveysets/1.json
  def destroy
    @surveyset.destroy
    respond_to do |format|
      format.html { redirect_to surveysets_url, notice: 'Surveyset was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_surveyset
      @surveyset = Surveyset.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def surveyset_params
      params.require(:surveyset).permit(:name, :full_name, :category, :description, :license, :fees, :intervals, :scoring, :link_to_questions, :user_id, :number_of_questions, :time_to_complete, :completion_rate, :condition_type, :care_plans)
    end
end
