Rails.application.routes.draw do
  root 'surveysets#index'
  get 'static_pages/home'
  get 'static_pages/help'
  get  '/secretsignup',  to: 'users#new'
  resources :surveysets
  resources :users
end
