require 'test_helper'

class SurveysetsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @surveyset = surveysets(:one)
  end

  test "should get index" do
    get surveysets_url
    assert_response :success
  end

  test "should get new" do
    get new_surveyset_url
    assert_response :success
  end

  test "should create surveyset" do
    assert_difference('Surveyset.count') do
      post surveysets_url, params: { surveyset: { category: @surveyset.category, description: @surveyset.description, fees: @surveyset.fees, full_name: @surveyset.full_name, intervals: @surveyset.intervals, license: @surveyset.license, link_to_questions: @surveyset.link_to_questions, name: @surveyset.name, scoring: @surveyset.scoring, user_id: @surveyset.user_id } }
    end

    assert_redirected_to surveyset_url(Surveyset.last)
  end

  test "should show surveyset" do
    get surveyset_url(@surveyset)
    assert_response :success
  end

  test "should get edit" do
    get edit_surveyset_url(@surveyset)
    assert_response :success
  end

  test "should update surveyset" do
    patch surveyset_url(@surveyset), params: { surveyset: { category: @surveyset.category, description: @surveyset.description, fees: @surveyset.fees, full_name: @surveyset.full_name, intervals: @surveyset.intervals, license: @surveyset.license, link_to_questions: @surveyset.link_to_questions, name: @surveyset.name, scoring: @surveyset.scoring, user_id: @surveyset.user_id } }
    assert_redirected_to surveyset_url(@surveyset)
  end

  test "should destroy surveyset" do
    assert_difference('Surveyset.count', -1) do
      delete surveyset_url(@surveyset)
    end

    assert_redirected_to surveysets_url
  end
end
