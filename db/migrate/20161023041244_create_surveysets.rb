class CreateSurveysets < ActiveRecord::Migration[5.0]
  def change
    create_table :surveysets do |t|
      t.string :name
      t.string :full_name
      t.string :category
      t.text :description
      t.text :license
      t.text :fees
      t.text :intervals
      t.text :scoring
      t.text :link_to_questions
      t.integer :user_id

      t.timestamps
    end
  end
end
