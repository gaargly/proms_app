class AddMoreColumnsToSurveysets < ActiveRecord::Migration[5.0]
  def change
    add_column :surveysets, :number_of_questions, :string
    add_column :surveysets, :time_to_complete, :string
    add_column :surveysets, :completion_rate, :string
    add_column :surveysets, :condition_type, :text
    add_column :surveysets, :care_plans, :text
  end
end
